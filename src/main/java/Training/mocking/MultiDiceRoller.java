package Training.mocking;

public class MultiDiceRoller {
	private DiceRoller diceRoller;

	public MultiDiceRoller(DiceRoller diceRoller) {
		this.diceRoller = diceRoller;
	}

	public double addDiceRolls(int numberOfRolls){
		Double result = 0.0;
		for (int i = 0; i < numberOfRolls; i++) {
			result +=  diceRoller.roll();
		}
		
		return result;
	}
}
