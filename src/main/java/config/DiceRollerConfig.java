package config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import Training.mocking.ConsistantDiceRoller;
import Training.mocking.DiceRoller;
import Training.mocking.MultiDiceRoller;
import Training.mocking.RandomDiceRoller;

@Configuration
@ComponentScan(basePackages={"Training.mocking"})
public class DiceRollerConfig {

	
	
	@Bean
	public DiceRoller getDiceRoller(){
		return new RandomDiceRoller();
	}
	
	
	@Bean
	public DiceRoller getDiceRollers(){
		return new ConsistantDiceRoller();
	}
	
	@Bean 
	public MultiDiceRoller getMultiDiceRoller(){
	
		return new MultiDiceRoller(getDiceRoller());
	}
}
