package app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import Training.mocking.MultiDiceRoller;
import config.DiceRollerConfig;

public class Application {
	
	
	
	 public static void main(String[] args)   {
		 ApplicationContext applicationContext = new AnnotationConfigApplicationContext(DiceRollerConfig.class);
		 
		 MultiDiceRoller multiDiceRoller = applicationContext.getBean(MultiDiceRoller.class);
		 
		 System.out.println("we rolled 5 dice and got: " + multiDiceRoller.addDiceRolls(5));
		 
	}	
}
