package Training.mocking;

import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

public class MultiDiceRollerTest {
	MultiDiceRoller multiDiceRoller;
	DiceRoller diceRoller;
	
	@Before
	public void init(){
		diceRoller = EasyMock.createMock(DiceRoller.class);
		multiDiceRoller = new MultiDiceRoller(diceRoller);
	}
	
	@Test
	public void canRollSingleDie(){
		EasyMock.expect(diceRoller.roll()).andReturn(1.0);
		EasyMock.replay(diceRoller);
		
		double result = multiDiceRoller.addDiceRolls(1);
		
		assertEquals(1, result,.01);
		EasyMock.verify(diceRoller);
	}
	
	@Test
	public void canRollMultipleDice(){
		EasyMock.expect(diceRoller.roll()).andReturn(1.0);
		EasyMock.expect(diceRoller.roll()).andReturn(4.0);
		EasyMock.replay(diceRoller);
		
		double result = multiDiceRoller.addDiceRolls(2);
		
		assertEquals(5, result,.01);
		EasyMock.verify(diceRoller);
	}
}
